<%@include file="index.jsp"%>
<html>
<head>
    <title>Login</title>
</head>
<body>
<h3>Login</h3>
<% String profile_msg = (String)request.getAttribute("profile_msg");
    if (profile_msg != null) {
        out.print(profile_msg);
    }

    String login_msg = (String)request.getAttribute("login_msg");
    if (login_msg != null) {
        out.print(login_msg);
    }
%>
<br/>

<form action="loginprocess.jsp" method="post">
    Username: <input type="text" name="username"><br/><br/>
    Password: <input type="password" name="password"><br/><br/>
    <input type="submit" value="Login">
</form>
</body>
</html>
