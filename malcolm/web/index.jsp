<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="java.util.logging.Level" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="java.sql.*" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Index</title>
</head>
<body>

<%!
    static {
        try {
            Class.forName ("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException ex) {}
    }
%>
<%
    try {
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@//larry.uopnet.plymouth.ac.uk:1521/orcl.fost.plymouth.ac.uk",
                "PRCS251A", "DamnDaniel"
        );
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM ROLES";
        ResultSet rs = stmt.executeQuery(query);
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        out.println("<table>");
        while (rs.next()) {
            out.println("<tr>");
            for (int i = 1; i <= columnsNumber; i++) {
                out.print("<td>" + rs.getString(i) + "</td>");
            }
            out.println("</tr>");
        }
        out.println("</table>");
    } catch (SQLException ex) {
        out.println(ex.getMessage());
    }
%>
<br/>
<a href="login.jsp">Login</a>|
<a href="logout.jsp">Logout</a>|
<a href="profile.jsp">Profile</a>
</body>
</html>
