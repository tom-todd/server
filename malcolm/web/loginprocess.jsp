<%@ page import="login.LoginDao" %>
<jsp:useBean id="obj" class="login.LoginDetails"/>

<jsp:setProperty property="*" name="obj"/>

<%
    boolean status = LoginDao.validate(obj);
    if (status){
        out.println("You are successfully logged in");
        session.setAttribute("session","TRUE");
        session.setAttribute("userName", obj.getUsername());
    }
    else
    {
        out.print("Sorry, email or password error");
    }
%>
<jsp:include page="index.jsp"/>