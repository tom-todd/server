package login;
import java.sql.*;
import static login.Provider.*;

public class ConnectionProvider {
    private static Connection connection = null;

    static {
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(CONNECTION_URL,USERNAME,PASSWORD);
        }   catch(Exception e){}
    }

    public static Connection getConnection(){
        return connection;
    }
}