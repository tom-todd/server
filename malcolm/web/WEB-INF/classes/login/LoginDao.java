package login;
import java.sql.*;

public class LoginDao {
    public static boolean validate(LoginDetails details) {
        boolean status = false;

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Users WHERE USERNAME=? AND PASSWORD=?");

            stmt.setString(1, details.getUsername());
            stmt.setString(2, details.getPassword());

            ResultSet rs = stmt.executeQuery();
            status = rs.next();
        } catch (Exception e) {}

        return status;
    }
}